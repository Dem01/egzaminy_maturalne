import urllib.request
import json

class Parse:
    regions = ["dolnoslaskie", "kujawsko-pomorskie", "lubelskie", "lubuskie", "łódzkie", "małopolskie", "mazowieckie",
              "opolskie", "podkarpackie", "podlaskie", "pomorskie", "śląskie", "świętokrzyskie", "warmińsko-mazurskie", "wielkopolskie", "zachodniopomorskie"]
    endpage = 32
    def __init__(self):
        self.data = self.save_all_data()
    
    def get(self):
        pprint(self.data)

    def save_all_data(self):    
        info = []
        link = "https://api.dane.gov.pl/resources/17363/data?page="
        for i in range(1, self.endpage):
            with urllib.request.urlopen(f"{link}{i}") as url:
                data = json.loads(url.read().decode())
                for i in data['data']:
                    info.append(i['attributes'])
        return info


class Voivodship(Parse):
    """

    definicje:
        col1 - column with voivodship names
        col2 - column with pass or didn't pass the examination
        col3 - column with gender of the examinated
        col4 - column up to and including the given year
        col5 - number of persons
    """    
    def __init__(self, data, voivodship, year, passed=True, gender=None):
        self.data = data
        self.voivodship = voivodship
        self.year = year
        self.gender = gender        
        

    def get(self, by_voivodship=False, only_passed=False, only_woman=False, only_man=False):
        if not only_passed and not by_voivodship:    
            for i in self.data:
                if i['col1'] == self.voivodship and i['col4'] <= self.year:
                    yield i
        elif only_passed and not by_voivodship:            
            for i in self.data:
                #wszyscy, którzy zdali
                if i['col2'] == "zdało" and not only_man and not only_woman:
                    yield i
                
                #tylko kobiety, które zdały
                elif i['col2'] == "zdało" and only_woman:
                    if i['col3'] == "kobiety":
                        yield i
                #tylko mężczyźni
                elif i['col2'] == "zdało" and only_man:
                    if i['col3'] == "mężczyźni":
                        yield i
        elif only_passed and by_voivodship:
            for i in self.data:
                #wszyscy, którzy zdali
                if i['col2'] == "zdało" and i['col4'] == self.year and i['col1'] == self.voivodship and not only_man and not only_woman:
                    yield i['col5']               
                #tylko kobiety, które zdały
                elif i['col2'] == "zdało" and i['col4'] == self.year and i['col1'] == self.voivodship and only_woman:
                    if i['col3'] == "kobiety":
                        yield i['col5']
                #tylko mężczyźni
                elif i['col2'] == "zdało" and i['col4'] == self.year and i['col1'] == self.voivodship and only_man:
                    if i['col3'] == "mężczyźni":
                        yield i['col5']

        else:
            for i in self.data:
                if i['col1'] == self.voivodship and i['col4'] <= self.year and i['col2'] == "zdało" and i['col3'] == self.gender:
                    yield i

    def get_to_db(self):
        for i in self.data:
            yield i

if __name__ == "__main__":     
    pass

    
    



