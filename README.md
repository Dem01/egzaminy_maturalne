## Matura 2010-2018, statystyki.

Based on these data: https://api.dane.gov.pl/resources/17363(Seems like it's not available anymore and I can't find similiar data :( )

How to run it: 
```
clone this repository
#in virtual env
pip install -r requirements.txt
python interface.py
```
example:
```
python interface.py 

loading data...
OK
1.Average number of persons who took the examination
2.Percentage pass rate for a given province
3.Highest pass rate in certain voivodship for a given year
4.Find a voivodship that has experienced a pass rate regression
5.Compare two voivodships
6.Load data to sqlite3 database
7.Read data from sqlite3 database
8.Remove table from sqlite3 database

Enter the number of the option

>>> 2
Type selected voivodship: podlaskie
Until the year: 2016
Distinguish data by gender? Type yes or no: no
2010 = 80.815%
2011 = 76.770%
2012 = 81.727%
2013 = 81.832%
2014 = 72.898%
2015 = 75.445%
2016 = 80.895%
```
