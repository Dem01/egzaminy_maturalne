import parsedata
import connect


def add_to_db(data):
        with connect.conn:                          
            user_data = parsedata.Voivodship(data, None, 2018).get_to_db()
            for i in user_data:
                voiv = i['col1']
                passe = i['col2']
                gender = i['col3']
                year = i['col4']
                quant = i['col5']
                connect.cursor.execute("""INSERT INTO matura VALUES(:voivodship, :passed, :sex, :year, :quantity)""",{'voivodship': voiv, 'passed': passe, 'sex': gender, 'year': year, 'quantity': quant}) 


def show_all():
    connect.cursor.execute("""SELECT * FROM matura""")
    fetch = connect.cursor.fetchall()
    for i in fetch:
        print("voivodship: {}\npassed: {}\nsex: {}\nyear: {}\nquantity: {}".format(i[0], i[1], i[2], i[3], i[4]))


def remove_all():
    """
    Function removes all records from the database
    """
    with connect.conn:
        connect.cursor.execute("DELETE FROM matura")
