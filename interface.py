from parsedata import Parse, Voivodship
import db
regions = ["dolnośląskie", "kujawsko-pomorskie", "lubelskie", "lubuskie", "łódzkie", "małopolskie", "mazowieckie",
           "opolskie", "podkarpackie", "podlaskie", "pomorskie", "śląskie", "świętokrzyskie", "warmińsko-mazurskie", "wielkopolskie", "zachodniopomorskie"]

def read_values():
    """
    Funkcja do sczytywania danych od użytkownika
    :params:
        None
    :return:
        user_voivodship.lower().capitalize() - województwo z pierwszą wielką literą
        year - rok 
    """
    try:
        user_voivodship = input("Type selected voivodship: ") #województwo
        year = int(input("Until the year: ")) # rok
    except:    
        print("Some error occured!")
    #trochę kontroli błędów     
    if user_voivodship.lower() not in regions:
        print("Incorrect voivodship name")
        raise ValueError       
    if year < 2010 or year > 2018:
        print("Incorrect year")
        raise ValueError        
    return user_voivodship.lower().capitalize(), year

def ask_for_gender():
    """
    Funkcja, która pozwala użytkownikowi wybrać czy chce 
    filtrować dane ze względu na płeć, jeśli tak, to którą płeć.
    :params:
        None
    :return:
        gender - zwraca płeć, którą użytkownik wybrał
    """   
    rv = input("Distinguish data by gender? Type yes or no: ") # return value
    if rv == 'yes':
        gender = input("Type the gender(kobieta or mężczyzna): ")
        if gender == 'kobieta' or gender == 'mężczyzna':
            return gender
        else:
            raise ValueError('Something went wrong..')            
    elif rv == 'no':
        return None
    else:
        raise ValueError
    

def main():


    #load all datas to variable(list)
    
    print("loading data...")
    load = True
    try:
        data = Parse().data        
    except:
        raise error("Data could not be downloaded from the server")
        
    print("1.Average number of persons who took the examination")
    print("2.Percentage pass rate for a given province")
    print("3.Highest pass rate in certain voivodship for a given year")
    print("4.Find a voivodship that has experienced a pass rate regression")
    print("5.Compare two voivodships")
    print("6.Load data to sqlite3 database")
    print("7.Read data from sqlite3 database")
    print("8.Remove table from sqlite3 database")
    print("OK")
    print("Enter the number of the option\n")
    #main loop
    while True:
        try:
            user = int(input(">>> "))
        except ValueError:
            print("Incorrect value!")
            continue
        except:
            print("Some error occured!")
            continue

        if user == 1:            
            suma = 0 #variable to keep track of summing data
            try:
                user_voivodship, year = read_values() #get voivodship and year from terminal
                gender = ask_for_gender()
            except:
                continue
            
            user_data = Voivodship(data, user_voivodship, year)#filter needed data      
            #obliczenie średniej ilości osób, które przystąpiły do egzaminu w danym województwie
            length_of_data = sum(1 for _ in user_data.get()) #liczba rekordów w danych
            if not gender: # jeśli gender = no
                for i in user_data.get():
                    if i['col2'] == 'przystąpiło':
                        #sumuje liczbę osób, które przystąpiły do egzaminu
                        suma += i['col5']            
                print(f"average number of persons who took the exam for a given voivodship({user_voivodship}) in years 2010 to {year} == {suma//length_of_data}")
            else:
                if gender == 'kobieta': #tym razem sumuję tylko liczbę kobiet, które przystąpiły do egzaminu
                    for i in user_data.get():
                        if i['col3'] == 'kobiety':
                            if i['col2'] == 'przystąpiło':
                                suma += i['col5']            
                    print(f"average number of women who took the exam for a given voivodship({user_voivodship}) in years 2010 to {year} == {suma//length_of_data}")
                elif gender == 'mężczyzna':# tutaj dla mężczyzn
                    for i in user_data.get():
                        if i['col3'] == 'mężczyźni':
                            if i['col2'] == 'przystąpiło':                            
                                suma += i['col5']            
                    print(f"average number of men who took the exam for a given voivodship({user_voivodship}) in years 2010 to {year} == {suma//length_of_data}")

            
        elif user == 2:
            #obliczenie procentowej zdawalności dla danego województwa
            
            try:
                #pobierz dane z terminala
                user_voivodship, year = read_values()
                gender = ask_for_gender()                
            except:
                continue
            user_data = Voivodship(data, user_voivodship, year) #zapisz juz przefiltrowane dane
            for j in range(2010, year+1):
                sum_passed = 0 #suma osob, ktore zdały egzamin
                sum_acceded = 0  # suma osob, ktore przystąpiły do egzaminu
                for i in user_data.get():
                    if not gender:
                        if i['col2'] == "zdało" and i['col4'] == j:
                            sum_passed += i['col5']
                        elif i['col2'] == "przystąpiło" and i['col4'] == j:
                            sum_acceded += i['col5']
                    elif gender == "kobieta":
                        if i['col2'] == "zdało" and i['col4'] == j and i['col3'] == "kobiety":
                            sum_passed += i['col5']
                        elif i['col2'] == "przystąpiło" and i['col4'] == j and i['col3'] == "kobiety":
                            sum_acceded += i['col5']
                    elif gender == "mężczyzna":
                        if i['col2'] == "zdało" and i['col4'] == j and i['col3'] == "mężczyźni":
                            sum_passed += i['col5']
                        elif i['col2'] == "przystąpiło" and i['col4'] == j and i['col3'] == "mężczyźni":
                            sum_acceded += i['col5']

                
                percentage = (sum_passed/sum_acceded)*100
                print(f"{j} = {percentage:.3f}%")

        elif user == 3:
            keep_track = 0 # zmienna potrzebna do śledzenia ilości powtórzeń pętli
            slownik = {}# slownik formatu {'wojewodztwo' : liczba osob, ktore zdaly test}
            lista = []
            try:
                #pobierz dane od uzytkownika
                year = int(input("Year: "))
                gender = ask_for_gender()
                if year < 2010 or year > 2018:
                    print("Incorrect year")
                    raise ValueError
            except:
                continue


            for voivodship in regions:
                #iteruje po wszystkich województwach i szukam rekordów, które 'zdały' i dla podanego roku                 
                user_data = Voivodship(data, voivodship.capitalize(), year)
                
                if gender:#sprawdzam czy uzytkownik chce wyswietlic dane tylko dla danej plci
                    if gender == 'kobieta':
                        user_data_gender = user_data.get(
                            only_passed=True, only_woman=True)
                    elif gender == 'mężczyzna':
                        user_data_gender = user_data.get(
                            only_passed=True, only_woman=False, only_man=True)
                elif not gender:
                    user_data_gender = user_data.get()
                for j in user_data_gender:
                    #przeszukuje wszystkie dane i te ktore odpowiadaja warunkom, podaje dalej
                    if j['col2'] == "zdało" and j['col4'] == year:                                               
                        lista.append(j['col5']) # do listy dodaje liczbę osob ktore zdały

                        keep_track += 1 # śledzę ile razy powtórzyła się dana pętla

                        #krótki skrypt, który sumuje liczbę kobiet i mężczyzn, 
                        #którzy zdali maturę dla odpowiadającego im regionu
                        if keep_track == 2:
                            #sumuję tylko dwa rekordy: dla danego województwa - kobiety + mężczyźni
                            suma = sum(lista)
                            #do słownika dodaję 'województwo' : liczba wszystkich osób, które zdały egzamin
                            slownik[voivodship] = suma
                            #resetuję listę oraz licznik
                            del(lista[:])
                            keep_track = 0
            max_pass_rate = max(slownik, key=slownik.get) # spośród listy szukam województwa o największej zdawalności
            print(slownik.get(max_pass_rate), max_pass_rate)
        elif user == 4:
            keep_track = 0 # zmienna do śledzenia ilości powtórzeń pętli
            lista = []
            try:
                #pobieram dane od użytkownika
                gender = ask_for_gender()
            except:
                continue
            for region in regions: #iteruje po wojewodztwach
                for year in range(2010, 2019): #oraz po roku                   
                    if gender: # sprawdzam co użytkownik wpisał i dostosowuję filtrację danych
                        if gender == 'kobieta':
                            user_data = Voivodship(data, region.capitalize(), year).get(
                                only_passed=True, by_voivodship=True, only_woman=True)
                        elif gender == 'mężczyzna':
                            user_data = Voivodship(data, region.capitalize(), year).get(
                                only_passed=True, by_voivodship=True, only_man=True)
                    elif not gender:
                        user_data = Voivodship(data, region.capitalize(), year).get(
                            only_passed=True, by_voivodship=True)

                    for i in user_data: #iteruję po danych                        
                        if not gender:
                            sum_of_men_and_women = i + next(user_data) #sumuję zdawalność kobiet oraz mężczyzn
                            lista.append(sum_of_men_and_women)
                            if len(lista) == 2: #gdy w liscie znajdą się dwa rekordy
                                if lista[0] > lista[1]: # sprawdzam czy nastąpiła regresja, tj zdawalność_wcześniej > zdawalnosć_później
                                    print(f"{region}: {year-1} -> {year}") 
                                del(lista[:])# przygotowuję listę dla kolejnych dwóch roków
                        elif gender: # tutaj to samo co wyżej, tylko dla wyznaczonej przez użytkownika płci
                            lista.append(i)
                            if len(lista) == 2:
                                if lista[0] > lista[1]:
                                    print(f"{region}: {year-1} -> {year}")
                                del(lista[:])

                    
 
        elif user == 5:
            try:
                #pobieram dwa województwa, które mam ze sobą porównać
                voivodship1 = input("Type first voivodship: ").lower()
                voivodship2 = input("Type second voivodship: ").lower()
                if voivodship1 not in regions or voivodship2 not in regions:
                    print("incorrect voivodship")
                    continue
                gender = ask_for_gender()
            except:
                continue
            for i in range(2010,2019):
                #sprawdzam jak użytkownik życzy sobie dostosować dane... wiem, muszę do przebudować
                if gender:
                    if gender == 'kobieta':
                        user_data1 = Voivodship(
                            data, voivodship1.capitalize(), i).get(by_voivodship=True, only_passed=True, only_woman=True)
                        user_data2 = Voivodship(
                            data, voivodship2.capitalize(), i).get(by_voivodship=True, only_passed=True, only_woman=True)

                    elif gender == 'mężczyzna':
                        user_data1 = Voivodship(
                            data, voivodship1.capitalize(), i).get(by_voivodship=True, only_passed=True, only_man=True)
                        user_data2 = Voivodship(
                            data, voivodship2.capitalize(), i).get(by_voivodship=True, only_passed=True, only_man=True)
                elif not gender:
                    user_data1 = Voivodship(
                        data, voivodship1.capitalize(), i).get(by_voivodship=True, only_passed=True)
                    user_data2 = Voivodship(
                        data, voivodship2.capitalize(), i).get(by_voivodship=True, only_passed=True)
                        
                max_user1 = max(user_data1) #maksymalna wartość zdawalności województwa1 spośród wybranych danych
                max_user2 = max(user_data2) #tutaj dla województwa drugiego 
                #sprawdzam, które województwo miało większą zdawalność
                if max_user1 > max_user2:
                    print(f"{i} - {voivodship1}")
                elif max_user1 < max_user2:
                    print(f"{i} - {voivodship2}")
                else:
                    print(f"{i} - equal")

        elif user == 6:
            db.add_to_db(data)                
        elif user == 7:
            db.show_all()
        elif user == 8:
            db.remove_all()
        else:
            print("I do not know that command")
            continue


if __name__ == "__main__":
    main()
